import React, { useEffect, useState} from 'react';

function ConferenceForm() {
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = presentations;
        data.max_attendees = attendees;
        data.location = location;
        console.log(data);
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type' : 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setPresentations('');
            setAttendees('');
            setLocation('');
        }
    }
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [presentations, setPresentations] = useState('');
    const [attendees, setAttendees] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }

    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handlePresentationsChange =(event) => {
        const value = event.target.value;
        setPresentations(value);
    }

    const handleAttendeesChange =(event) => {
        const value = event.target.value;
        setAttendees(value);
    }

    const handleLocationChange =(event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit}  id="create-conference-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleStartsChange} value={starts} placeholder="MM/DD/YYYY" required type="date" name="starts" id="starts" className="form-control"/>
                    <label htmlFor="date">Start date</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleEndsChange} value={ends} placeholder="MM/DD/YYYY" required type="date" name="ends" id="ends" className="form-control"/>
                    <label htmlFor="date">End date</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="description" className="form-label">Description</label>
                    <textarea onChange={handleDescriptionChange} value={description} className="form-control" id="description" name="description" rows="3"></textarea>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handlePresentationsChange} value={presentations} placeholder="Max Presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control"/>
                    <label htmlFor="max_presentations">Max Presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleAttendeesChange} value={attendees} placeholder="Max Attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control"/>
                    <label htmlFor="max_attendees">Max Attendees</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleLocationChange} value ={location} required id="location" name="location" className="form-select">
                    <option value="">Choose a location </option>
                    {locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>{location.name} </option>
                        );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>
    );

}

export default ConferenceForm;
