function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="col">
        <div class="card shadow">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                ${starts} - ${ends}
            </div>
        </div>
    </div>
    `;
}


function displayAlert(message,type) {
    const alert = document.createElement("div");
    alert.innerHTML =
        '<div class="alert mb-0 alert-' +
        type +
        ' alert-dismissible" role="alert">' +
        message +
        '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>';
    const alertPlaceholder = document.getElementById("liveAlertPlaceholder");
    alertPlaceholder.appendChild(alert);
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';


    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error(response.status);
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts);
                    const ends = new Date(details.conference.ends);
                    const location = details.conference.location.name;
                    const html = createCard(name, description, pictureUrl, starts.toLocaleDateString(), ends.toLocaleDateString(), location);
                    const row = document.querySelector(".row");
                    row.innerHTML += html;
                }
            }
        }
    } catch (e) {
        alert(`${e} something not so chill just happened :(`, "danger");
        console.error("error", e)
    }
});
