
//Add an event listener for when the DOM loads
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';
    try{
        const response = await fetch(url);
        if (!response.ok){
            throw new Error(response.status);
        } else {
            const data = await response.json();
            const selectTag = document.getElementById('location');
            for(let location of data.locations) {
                const option = document.createElement('option');
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.appendChild(option);

            }
        }
    } catch (e) {
        console.error("error" ,e);
    }

    const formTag = document.getElementById("create-conference-form");
    formTag.addEventListener("submit", async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json)
        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method : "post",
            body : json,
            header: {"Content-Type" : "application/json"}
        }
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference)
        }
    })
})
